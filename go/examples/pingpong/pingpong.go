// Copyright 2017 ETH Zurich
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Simple application for SCION connectivity using the snet library.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"math"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	log "github.com/inconshreveable/log15"
	"github.com/lucas-clemente/quic-go"
	"github.com/lucas-clemente/quic-go/qerr"

	"github.com/scionproto/scion/go/lib/addr"
	"github.com/scionproto/scion/go/lib/common"
	liblog "github.com/scionproto/scion/go/lib/log"
	sd "github.com/scionproto/scion/go/lib/sciond"
	"github.com/scionproto/scion/go/lib/snet"
	"github.com/scionproto/scion/go/lib/snet/squic"
	"github.com/scionproto/scion/go/lib/spath"
	"github.com/scionproto/scion/go/lib/pathmgr"
)

const (
	DefaultInterval = 1 * time.Second
	DefaultTimeout  = 2 * time.Second
	MaxPings        = 1 << 16
	ReqMsg          = "ping!"
	ReplyMsg        = "pong!"
	TSLen           = 8
)

func GetDefaultSCIONDPath(ia addr.IA) string {
	return fmt.Sprintf("/run/shm/sciond/sd%v.sock", ia)
}

var (
	local       snet.Addr
	remote      snet.Addr
	interactive = flag.Bool("i", false, "Interactive mode")
	id          = flag.String("id", "pingpong", "Element ID")
	mode        = flag.String("mode", "client", "Run in client or server mode")
	sciond      = flag.String("sciond", "", "Path to sciond socket")
	dispatcher  = flag.String("dispatcher", "/run/shm/dispatcher/default.sock",
		"Path to dispatcher socket")
	count = flag.Int("count", 0,
		fmt.Sprintf("Number of pings, between 0 and %d; a count of 0 means infinity", MaxPings))
	timeout = flag.Duration("timeout", DefaultTimeout,
		"Timeout for the ping response")
	interval     = flag.Duration("interval", DefaultInterval, "time between pings")
	verbose  = flag.Bool("v", false, "sets verbose output")
	formatString = flag.String("format", "", "Format string use to format output, e.g. \"%[9]s : %3.3[5]s\"")
	aggregate = flag.Bool("aggregate", false, "Aggregate output only at end of run")
	policyReq  = flag.String("policyReq", "0-0#0;long", "Path policy to require ISD-AS, " +
		"use short to filter out circular paths and long to show all matching paths. " +
		"0 acts as a wildcard for ISD, AS and interface specification.")
	policyDen  = flag.String("policyDen", "1-1#111;long", "Path policy to deny ISD-AS , " +
		"use short to filter out circular paths and long to show all matching paths. " +
		"0 acts as a wildcard for ISD, AS and interface specification. By default the unused AS 1-1 is denied.")
	pathAlgo   = flag.String("pathAlgo", "", "Path selection algorithm / metric (\"shortest\", \"mtu\")")
)

func init() {
	flag.Var((*snet.Addr)(&local), "local", "(Mandatory) address to listen on")
	flag.Var((*snet.Addr)(&remote), "remote", "(Mandatory for clients) address to connect to")
}

func main() {
	liblog.AddDefaultLogFlags()
	validateFlags()
	liblog.Setup(*id)
	defer liblog.LogPanicAndExit()
	switch *mode {
	case "client":
		if remote.Host == nil {
			LogFatal("Missing remote address")
		}
		if remote.L4Port == 0 {
			LogFatal("Invalid remote port", "remote port", remote.L4Port)
		}
		Client()
	case "server":
		Server()
	}
}

func validateFlags() {
	flag.Parse()
	if *mode != "client" && *mode != "server" {
		LogFatal("Unknown mode, must be either 'client' or 'server'")
	}
	if *mode == "client" && remote.Host == nil {
		LogFatal("Missing remote address")
	}
	if local.Host == nil {
		LogFatal("Missing local address")
	}
	if *sciond == "" {
		*sciond = GetDefaultSCIONDPath(local.IA)
	}
	if *count < 0 || *count > MaxPings {
		LogFatal("Invalid count", "min", 0, "max", MaxPings, "actual", *count)
	}
}

// Client dials to a remote SCION address and repeatedly sends ping messages
// while receiving pong messages. For each successful ping-pong, a message
// with the round trip time is printed. On errors (including timeouts),
// the Client exits.
func Client() {
	initNetwork()

	// Needs to happen before DialSCION, as it will 'copy' the remote to the connection.
	// If remote is not in local AS, we need a path!
	if !remote.IA.Eq(local.IA) {
		pathEntry := choosePath(*interactive)
		if pathEntry == nil {
			LogFatal("No paths available to remote destination")
		}
		remote.Path = spath.New(pathEntry.Path.FwdPath)
		remote.Path.InitOffsets()
		remote.NextHopHost = pathEntry.HostInfo.Host()
		remote.NextHopPort = pathEntry.HostInfo.Port
	}

	// Connect to remote address. Note that currently the SCION library
	// does not support automatic binding to local addresses, so the local
	// IP address needs to be supplied explicitly. When supplied a local
	// port of 0, DialSCION will assign a random free local port.
	qsess, err := squic.DialSCION(nil, &local, &remote)
	if err != nil {
		LogFatal("Unable to dial", "err", err)
	}
	defer qsess.Close(nil)

	qstream, err := qsess.OpenStreamSync()
	if err != nil {
		LogFatal("quic OpenStream failed", "err", err)
	}
	defer qstream.Close()
	log.Debug("Quic stream opened", "local", &local, "remote", &remote)
	go Send(qstream)
	Read(qstream)
}

func Send(qstream quic.Stream) {
	reqMsgLen := len(ReqMsg)
	payload := make([]byte, reqMsgLen+TSLen)
	copy(payload[0:], ReqMsg)
	for i := 0; i < *count || *count == 0; i++ {
		if i != 0 && *interval != 0 {
			time.Sleep(*interval)
		}

		// Send ping message to destination
		before := time.Now()
		common.Order.PutUint64(payload[reqMsgLen:], uint64(before.UnixNano()))
		written, err := qstream.Write(payload[:])
		if err != nil {
			qer := qerr.ToQuicError(err)
			if qer.ErrorCode == qerr.NetworkIdleTimeout {
				log.Debug("The connection timed out due to no network activity")
				break
			}
			log.Error("Unable to write", "err", err)
			continue
		}
		if written != len(ReqMsg)+TSLen {
			log.Error("Wrote incomplete message", "expected", len(ReqMsg)+TSLen,
				"actual", written)
			continue
		}
	}
	// After sending the last ping, set a ReadDeadline on the stream
	err := qstream.SetReadDeadline(time.Now().Add(*timeout))
	if err != nil {
		LogFatal("SetReadDeadline failed", "err", err)
	}
}

func Read(qstream quic.Stream) {
	// Receive pong message (with final timeout)
	b := make([]byte, 1<<12)
	var RTTs []time.Duration
	readTotal := 0
	replyMsgLen := len(ReplyMsg)
	for i := 0; i < *count || *count == 0; i++ {
		read, err := qstream.Read(b)
		after := time.Now()
		if err != nil {
			qer := qerr.ToQuicError(err)
			if qer.ErrorCode == qerr.PeerGoingAway {
				log.Debug("Quic peer disconnected")
				break
			}
			if nerr, ok := err.(net.Error); ok && nerr.Timeout() {
				log.Debug("ReadDeadline missed", "err", err)
				// ReadDeadline is only set after we are done writing
				// and we don't want to wait indefinitely for the remaining responses
				break
			}
			log.Error("Unable to read", "err", err)
			continue
		}
		if read < replyMsgLen || string(b[:replyMsgLen]) != ReplyMsg {
			fmt.Println("Received bad message", "expected", ReplyMsg,
				"actual", string(b[:read]))
			continue
		}
		if read < replyMsgLen+TSLen {
			fmt.Println("Received bad message missing timestamp",
				"actual", string(b[:read]))
			continue
		}
		before := time.Unix(0, int64(common.Order.Uint64(b[replyMsgLen:replyMsgLen+TSLen])))
		elapsed := after.Sub(before)
		RTTs = append(RTTs, elapsed)
		readTotal += read
		if !*aggregate {
			if *verbose {
				fmt.Printf("[%d]\t", before.Unix())
			}
			if *formatString != "" {
				fmt.Printf(*formatString+"\n",
					before.Unix(), read, &remote, i, elapsed, before, after, local.Host, remote.Host, timeout, interval)
			} else {
				fmt.Printf("Received %d bytes from %v: seq=%d RTT=%s\n",
					read, &remote, i, elapsed)
			}
		} else if i == *count-1 {
			var RTTsMs []float64 // RTTs in ms
			for _, RTT := range RTTs {
				RTTsMs = append(RTTsMs, float64(RTT.Round(time.Nanosecond))/1e6)
			}
			RTTsString := fmt.Sprintf("%.2f", RTTsMs)
			RTTsString = RTTsString[1 : len(RTTsString)-1]
			fmt.Printf(*formatString+"\n", after.Unix(), readTotal,
				&remote, i, RTTsString, local.Host, remote.Host, timeout, interval)
		}
	}
}

// Server listens on a SCION address and replies to any ping message.
// On any error, the server exits.
func Server() {
	initNetwork()

	// Listen on SCION address
	qsock, err := squic.ListenSCION(nil, &local)
	if err != nil {
		LogFatal("Unable to listen", "err", err)
	}
	log.Debug("Listening", "local", qsock.Addr())
	for {
		qsess, err := qsock.Accept()
		if err != nil {
			log.Error("Unable to accept quic session", "err", err)
			continue
		}
		log.Debug("Quic session accepted", "src", qsess.RemoteAddr())
		go handleClient(qsess)
	}
}

func initNetwork() {
	// Initialize default SCION networking context
	if err := snet.Init(local.IA, *sciond, *dispatcher); err != nil {
		LogFatal("Unable to initialize SCION network", "err", err)
	}
	log.Debug("SCION network successfully initialized")
	if err := squic.Init("", ""); err != nil {
		LogFatal("Unable to initialize QUIC/SCION", "err", err)
	}
	log.Debug("QUIC/SCION successfully initialized")
}

func handleClient(qsess quic.Session) {
	defer qsess.Close(nil)
	qstream, err := qsess.AcceptStream()
	defer qstream.Close()
	if err != nil {
		log.Error("Unable to accept quic stream", "err", err)
		return
	}

	b := make([]byte, 1<<12)
	reqMsgLen := len(ReqMsg)
	for {
		// Receive ping message
		read, err := qstream.Read(b)
		if err != nil {
			qer := qerr.ToQuicError(err)
			if qer.ErrorCode == qerr.PeerGoingAway {
				log.Debug("Quic peer disconnected")
				break
			}
			log.Error("Unable to read", "err", err)
			break
		}
		if string(b[:reqMsgLen]) != ReqMsg {
			fmt.Println("Received bad message", "expected", ReqMsg,
				"actual", string(b[:reqMsgLen]), "full", string(b[:read]))
		}
		// extract timestamp
		ts := common.Order.Uint64(b[reqMsgLen:])

		// Send pong message
		replyMsgLen := len(ReplyMsg)
		copy(b[:replyMsgLen], ReplyMsg)
		common.Order.PutUint64(b[replyMsgLen:], ts)
		written, err := qstream.Write(b[:replyMsgLen+TSLen])
		if err != nil {
			log.Error("Unable to write", "err", err)
			continue
		} else if written != len(ReplyMsg)+TSLen {
			log.Error("Wrote incomplete message",
				"expected", len(ReplyMsg)+TSLen, "actual", written)
			continue
		}
	}
}

func LogFatal(msg string, a ...interface{}) {
	log.Crit(msg, a...)
	os.Exit(1)
}

func policyFilterPaths(appPaths pathmgr.AppPathSet) pathmgr.AppPathSet {
	policyDenied := strings.Split(*policyDen, ";")
	pp, err := pathmgr.NewPathPredicate(policyDenied[0])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to parse path reply: %v\n", err)
	}
	for _, ap := range appPaths {
		if pp.Eval(ap.Entry) {
			delete(appPaths, ap.Key())
		}
	}

	policyRequired := strings.Split(*policyReq, ";")
	pp, err = pathmgr.NewPathPredicate(policyRequired[0])
	for _, ap := range appPaths {
		if !pp.Eval(ap.Entry) {
			delete(appPaths, ap.Key())
		}
	}

	// remove unshortened paths, if policy is set to "short"
	if (len(policyDenied) > 1 && policyDenied[1] == "short") ||
		(len(policyRequired) > 1 && policyRequired[1] == "short") {
		var longPaths []pathmgr.PathKey
		for k, p := range appPaths {
			IAs := make(map[addr.IA]int)
			for _, fwpm := range p.Entry.Path.Interfaces {
				IAs[fwpm.ISD_AS()] += 1
				if IAs[fwpm.ISD_AS()] > 2 {
					longPaths = append(longPaths, k)
				}
			}
		}
		for _, k := range longPaths {
			delete(appPaths, k)
		}
	}
	return appPaths
}

func choosePath(interactive bool) *sd.PathReplyEntry {
	var paths []*sd.PathReplyEntry
	var pathIndex uint64

	pathMgr := snet.DefNetwork.PathResolver()
	pathSet := pathMgr.Query(local.IA, remote.IA)
	pathSet = policyFilterPaths(pathSet)

	if len(pathSet) == 0 {
		return nil
	}

	// check if pathAlgo flag is set
	flagset := make(map[string]bool)
	// record if flags were set or if default value was used
	flag.Visit(func(f *flag.Flag) { flagset[f.Name] = true })

	if flagset["pathAlgo"] {
		aps := pathmgr.AppPathSet{}
		bestPath := pathSelection(pathSet, *pathAlgo)
		aps.Add(bestPath.Entry)
		pathSet = aps
	}

	for _, p := range pathSet {
		paths = append(paths, p.Entry)
	}
	if interactive {
		fmt.Printf("Available paths to %v\n", remote.IA)
		for i := range paths {
			fmt.Printf("[%2d] %s\n", i, paths[i].Path.String())
		}
		reader := bufio.NewReader(os.Stdin)
		for {
			fmt.Printf("Choose path: ")
			pathIndexStr, _ := reader.ReadString('\n')
			var err error
			pathIndex, err = strconv.ParseUint(pathIndexStr[:len(pathIndexStr)-1], 10, 64)
			if err == nil && int(pathIndex) < len(paths) {
				break
			}
			fmt.Fprintf(os.Stderr, "ERROR: Invalid path index, valid indices range: [0, %v]\n", len(paths))
		}
		fmt.Printf("Using path:\n  %s\n", paths[pathIndex].Path.String())
	}
	return paths[pathIndex]
}

func pathSelection(pathSet pathmgr.AppPathSet, pathAlgo string) *pathmgr.AppPath {
	var selectedPath *pathmgr.AppPath
	var metric float64
	// A path selection algorithm consists of a simple comparision function selecting the best path according
	// to some path property and a metric function normalizing that property to a value in [0,1], where larger is better
	// Available path selection algorithms, the metric returned must be normalized between [0,1]:
	pathAlgos := map[string](func(pathmgr.AppPathSet) (*pathmgr.AppPath, float64)){
		"shortest": selectShortestPath,
		"mtu": selectLargestMTUPath,
	}
	switch pathAlgo {
	case "shortest":
		selectedPath, metric = pathAlgos[pathAlgo](pathSet)
	case "mtu":
		selectedPath, metric = pathAlgos[pathAlgo](pathSet)
	default:
		// Default is to take result with best score
		for _, algo := range pathAlgos {
			cadidatePath, cadidateMetric := algo(pathSet)
			if cadidateMetric > metric {
				selectedPath = cadidatePath
				metric = cadidateMetric
			}
		}
	}
	return selectedPath
}

func selectShortestPath(pathSet pathmgr.AppPathSet) (selectedPath *pathmgr.AppPath, metric float64) {
	// Selects shortest path by number of hops
	for _, appPath := range pathSet {
		if selectedPath == nil || len(appPath.Entry.Path.Interfaces) < len(selectedPath.Entry.Path.Interfaces) {
			selectedPath = appPath
		}
	}
	metric_fn := func(rawMetric []sd.PathInterface) (result float64) {
		hopCount := float64(len(rawMetric))
		midpoint := 7.0
		result = math.Exp(-(hopCount-midpoint)) / (1 + math.Exp(-(hopCount-midpoint)))
		return result
	}
	return selectedPath, metric_fn(selectedPath.Entry.Path.Interfaces)
}

func selectLargestMTUPath(pathSet pathmgr.AppPathSet) (selectedPath *pathmgr.AppPath, metric float64) {
	// Selects path with largest MTU
	for _, appPath := range pathSet {
		if selectedPath == nil || appPath.Entry.Path.Mtu > selectedPath.Entry.Path.Mtu {
			selectedPath = appPath
		}
	}
	metric_fn := func(rawMetric uint16) (result float64) {
		mtu := float64(rawMetric)
		midpoint := 1500.0
		tilt := 0.004
		result = 1 / (1 + math.Exp(-tilt*(mtu-midpoint)))
		return result
	}
	return selectedPath, metric_fn(selectedPath.Entry.Path.Mtu)
}
