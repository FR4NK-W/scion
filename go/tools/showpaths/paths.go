// Copyright 2018 ETH Zurich
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Simple show paths application for SCION.
package main

import (
	"flag"
	"fmt"
	"math"
	"os"
	"strings"
	"time"

	"github.com/scionproto/scion/go/lib/addr"
	"github.com/scionproto/scion/go/lib/pathmgr"
	"github.com/scionproto/scion/go/lib/sciond"
)

var (
	dstIAStr   = flag.String("dstIA", "", "Destination IA address: ISD-AS")
	srcIAStr   = flag.String("srcIA", "", "Source IA address: ISD-AS")
	sciondPath = flag.String("sciond", "", "SCIOND socket path")
	timeout    = flag.Duration("timeout", 2*time.Second, "SCIOND connection timeout")
	maxPaths   = flag.Int("maxpaths", 10, "Maximum number of paths")
	policyReq  = flag.String("policyReq", "0-0#0;long", "Path policy to require ISD-AS, " +
		"use short to filter out circular paths and long to show all matching paths. " +
		"0 acts as a wildcard for ISD, AS and interface specification.")
	policyDen  = flag.String("policyDen", "1-1#111;long", "Path policy to deny ISD-AS , " +
		"use short to filter out circular paths and long to show all matching paths. " +
		"0 acts as a wildcard for ISD, AS and interface specification. By default the unused AS 1-1 is denied.")
	pathAlgo   = flag.String("pathAlgo", "", "Path selection algorithm / metric (\"shortest\", \"mtu\")")
)

var (
	dstIA addr.IA
	srcIA addr.IA
)

func main() {
	var err error

	validateFlags()

	sd := sciond.NewService(*sciondPath)
	sdConn, err := sd.ConnectTimeout(*timeout)
	sdConn.SetDeadline(time.Now().Add(*timeout))
	if err != nil {
		LogFatal("Failed to connect to SCIOND: %v\n", err)
	}
	reply, err := sdConn.Paths(dstIA, srcIA, uint16(*maxPaths), sciond.PathReqFlags{})
	if err != nil {
		LogFatal("Failed to retrieve paths from SCIOND: %v\n", err)
	}

	appPaths := policyFilterPaths(reply)

	// check if pathAlgo flag is set
	flagset := make(map[string]bool)
	// record if flags were set or if default value was used
	flag.Visit(func(f *flag.Flag) { flagset[f.Name] = true })

	if flagset["pathAlgo"] {
		aps := pathmgr.AppPathSet{}
		bestPath := pathSelection(appPaths, *pathAlgo)
		aps.Add(bestPath.Entry)
		appPaths = aps
	}


	fmt.Println("Best matching path(s) to", dstIA)
	i := 0
	for _, p := range appPaths {
		fmt.Printf("[%2d] %s\n", i, p.Entry.Path.String())
		i++
	}
}

func policyFilterPaths(reply *sciond.PathReply) (appPaths pathmgr.AppPathSet) {
	appPaths = pathmgr.NewAppPathSet(reply)

	policyDenied := strings.Split(*policyDen, ";")
	pp, err := pathmgr.NewPathPredicate(policyDenied[0])
	if err != nil {
		LogFatal("Unable to parse path reply: %v\n", err)
	}
	for _, ap := range appPaths {
		if pp.Eval(ap.Entry) {
			delete(appPaths, ap.Key())
		}
	}

	policyRequired := strings.Split(*policyReq, ";")
	pp, err = pathmgr.NewPathPredicate(policyRequired[0])
	for _, ap := range appPaths {
		if !pp.Eval(ap.Entry) {
			delete(appPaths, ap.Key())
		}
	}

	// remove unshortened paths, if policy is set to "short"
	if (len(policyDenied) > 1 && policyDenied[1] == "short") ||
		(len(policyRequired) > 1 && policyRequired[1] == "short") {
		var longPaths []pathmgr.PathKey
		for k, p := range appPaths {
			IAs := make(map[addr.IA]int)
			for _, fwpm := range p.Entry.Path.Interfaces {
				IAs[fwpm.ISD_AS()] += 1
				if IAs[fwpm.ISD_AS()] > 2 {
					longPaths = append(longPaths, k)
				}
			}
		}
		for _, k := range longPaths {
			delete(appPaths, k)
		}
	}
	return
}

func pathSelection(pathSet pathmgr.AppPathSet, pathAlgo string) *pathmgr.AppPath {
	var selectedPath *pathmgr.AppPath
	var metric float64
	// A path selection algorithm consists of a simple comparision function selecting the best path according
	// to some path property and a metric function normalizing that property to a value in [0,1], where larger is better
	// Available path selection algorithms, the metric returned must be normalized between [0,1]:
	pathAlgos := map[string](func(pathmgr.AppPathSet) (*pathmgr.AppPath, float64)){
		"shortest": selectShortestPath,
		"mtu": selectLargestMTUPath,
	}
	switch pathAlgo {
	case "shortest":
		selectedPath, metric = pathAlgos[pathAlgo](pathSet)
	case "mtu":
		selectedPath, metric = pathAlgos[pathAlgo](pathSet)
	default:
		// Default is to take result with best score
		for _, algo := range pathAlgos {
			cadidatePath, cadidateMetric := algo(pathSet)
			if cadidateMetric > metric {
				selectedPath = cadidatePath
				metric = cadidateMetric
			}
		}
	}
	return selectedPath
}

func selectShortestPath(pathSet pathmgr.AppPathSet) (selectedPath *pathmgr.AppPath, metric float64) {
	// Selects shortest path by number of hops
	for _, appPath := range pathSet {
		if selectedPath == nil || len(appPath.Entry.Path.Interfaces) < len(selectedPath.Entry.Path.Interfaces) {
			selectedPath = appPath
		}
	}
	metric_fn := func(rawMetric []sciond.PathInterface) (result float64) {
		hopCount := float64(len(rawMetric))
		midpoint := 7.0
		result = math.Exp(-(hopCount-midpoint)) / (1 + math.Exp(-(hopCount-midpoint)))
		return result
	}
	return selectedPath, metric_fn(selectedPath.Entry.Path.Interfaces)
}

func selectLargestMTUPath(pathSet pathmgr.AppPathSet) (selectedPath *pathmgr.AppPath, metric float64) {
	// Selects path with largest MTU
	for _, appPath := range pathSet {
		if selectedPath == nil || appPath.Entry.Path.Mtu > selectedPath.Entry.Path.Mtu {
			selectedPath = appPath
		}
	}
	metric_fn := func(rawMetric uint16) (result float64) {
		mtu := float64(rawMetric)
		midpoint := 1500.0
		tilt := 0.004
		result = 1 / (1 + math.Exp(-tilt*(mtu-midpoint)))
		return result
	}
	return selectedPath, metric_fn(selectedPath.Entry.Path.Mtu)
}

func validateFlags() {
	var err error

	flag.Parse()

	dstIA, err = addr.IAFromString(*dstIAStr)
	if err != nil {
		LogFatal("Unable to parse destination IA: %v\n", err)
	}
	if *sciondPath == "" {
		if *srcIAStr == "" {
			*sciondPath = "/run/shm/sciond/default.sock"
		} else {
			*sciondPath = "/run/shm/sciond/sd" + *srcIAStr + ".sock"
		}
	} else if *srcIAStr != "" {
		fmt.Printf("srcIA ignored! sciond takes precedence\n")
	}
	if *srcIAStr == "" {
		// Set any value, required by Query() but does not affect result
		*srcIAStr = "1-10"
	}
	srcIA, err = addr.IAFromString(*srcIAStr)
	if err != nil {
		LogFatal("Unable to parse source IA: %v\n", err)
	}
}

func LogFatal(msg string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, msg, a...)
	os.Exit(1)
}
