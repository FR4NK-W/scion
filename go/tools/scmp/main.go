// Copyright 2018 ETH Zurich
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Simple echo application for SCION connectivity tests.
package main

import (
	"bufio"
	"flag"
	"fmt"
	"math"
	"math/rand"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/scionproto/scion/go/lib/addr"
	"github.com/scionproto/scion/go/lib/common"
	"github.com/scionproto/scion/go/lib/hpkt"
	"github.com/scionproto/scion/go/lib/overlay"
	"github.com/scionproto/scion/go/lib/sciond"
	"github.com/scionproto/scion/go/lib/scmp"
	"github.com/scionproto/scion/go/lib/snet"
	"github.com/scionproto/scion/go/lib/sock/reliable"
	"github.com/scionproto/scion/go/lib/spath"
	"github.com/scionproto/scion/go/lib/pathmgr"
)

const (
	DefaultInterval = 1 * time.Second
	DefaultTimeout  = 2 * time.Second
	MaxEchoes       = 1 << 16
)

func GetDefaultSCIONDPath(ia addr.IA) string {
	return fmt.Sprintf("/run/shm/sciond/sd%v.sock", ia)
}

var (
	id          = flag.String("id", "echo", "Element ID")
	interactive = flag.Bool("i", false, "Interactive mode")
	sciondPath  = flag.String("sciond", "", "Path to sciond socket")
	dispatcher  = flag.String("dispatcher", "/run/shm/dispatcher/default.sock",
		"Path to dispatcher socket")
	interval  = flag.Duration("interval", DefaultInterval, "time between packets")
	timeout   = flag.Duration("timeout", DefaultTimeout, "timeout per packet")
	count     = flag.Uint("c", 0, "Total number of packet to send (ignored if not echo)")
	sTypeStr  = flag.String("t", "echo", "SCMP Type: echo |  rp | recordpath")
	policyReq  = flag.String("policyReq", "0-0#0;long", "Path policy to require ISD-AS, " +
		"use short to filter out circular paths and long to show all matching paths. " +
		"0 acts as a wildcard for ISD, AS and interface specification.")
	policyDen  = flag.String("policyDen", "1-1#111;long", "Path policy to deny ISD-AS , " +
		"use short to filter out circular paths and long to show all matching paths. " +
		"0 acts as a wildcard for ISD, AS and interface specification. By default the unused AS 1-1 is denied.")
	pathAlgo   = flag.String("pathAlgo", "", "Path selection algorithm / metric (\"shortest\", \"mtu\")")
	verbose = flag.Bool("verbose", false, "Be more verbose about what is going on")
	probeAll = flag.Bool("probeAll", false, "Probe all available paths, disables all filtering options")
	local     snet.Addr
	remote    snet.Addr
	bind      snet.Addr
	rnd       *rand.Rand
	pathEntry *sciond.PathReplyEntry
	pathStr   string
	mtu       uint16
)

var sType string = "echo"

func init() {
	flag.Var((*snet.Addr)(&local), "local", "(Mandatory) address to listen on")
	flag.Var((*snet.Addr)(&remote), "remote", "(Mandatory for clients) address to connect to")
	flag.Var((*snet.Addr)(&bind), "bind", "address to bind to, if running behind NAT")
}

func main() {
	var wg sync.WaitGroup

	flag.Parse()
	validate()

	if *sciondPath == "" {
		*sciondPath = GetDefaultSCIONDPath(local.IA)
	}
	// Initialize default SCION networking context
	if err := snet.Init(local.IA, *sciondPath, *dispatcher); err != nil {
		fatal("Unable to initialize SCION network\nerr=%v", err)
	}
	// Connect directly to the dispatcher
	address := &reliable.AppAddr{Addr: local.Host}
	var bindAddress *reliable.AppAddr
	if bind.Host != nil {
		bindAddress = &reliable.AppAddr{Addr: bind.Host}
	}
	conn, _, err := reliable.Register(*dispatcher, local.IA, address, bindAddress, addr.SvcNone)
	if err != nil {
		fatal("Unable to register with the dispatcher addr=%s\nerr=%v", local, err)
	}
	defer conn.Close()

	var paths []*sciond.PathReplyEntry
	if !*probeAll {
		pathEntry = choosePath(*interactive)
		paths = append(paths, pathEntry)
	} else {
		pathMgr := snet.DefNetwork.PathResolver()
		pathSet := pathMgr.Query(local.IA, remote.IA)
		for _, p := range pathSet {
			paths = append(paths, p.Entry)
		}
	}

	ret := 0
	for _, pathEntry := range paths {
		pathStr = pathEntry.Path.String()
		// If remote is not in local AS, we need a path!
		if !remote.IA.Eq(local.IA) {
			mtu = setPathAndMtu(pathEntry)
		} else {
			mtu = setLocalMtu()
		}
		seed := rand.NewSource(time.Now().UnixNano())
		rnd = rand.New(seed)

		var ctx scmpCtx
		initSCMP(&ctx, *sTypeStr, *count, pathEntry)

		ch := make(chan time.Time, 20)
		wg.Add(2)
		go RecvPkts(&wg, conn, &ctx, ch)
		go SendPkts(&wg, conn, &ctx, ch)

		wg.Wait()
		if ctx.sent != ctx.recv {
			ret += 1
		}
	}

	os.Exit(ret)
}

func SendPkts(wg *sync.WaitGroup, conn *reliable.Conn, ctx *scmpCtx, ch chan time.Time) {
	defer wg.Done()
	defer close(ch)

	b := make(common.RawBytes, mtu)

	nhAddr := reliable.AppAddr{Addr: remote.NextHopHost, Port: remote.NextHopPort}
	if remote.NextHopHost == nil {
		nhAddr = reliable.AppAddr{Addr: remote.Host, Port: overlay.EndhostPort}
	}
	nextPktTS := time.Now()
	ticker := time.NewTicker(*interval)
	for ; true; nextPktTS = <-ticker.C {
		updatePktTS(ctx, nextPktTS)
		// Serialize packet to internal buffer
		pktLen, err := hpkt.WriteScnPkt(ctx.pktS, b)
		if err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: Unable to serialize SCION packet %v\n", err)
			break
		}
		written, err := conn.WriteTo(b[:pktLen], &nhAddr)
		if err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: Unable to write %v\n", err)
			break
		} else if written != pktLen {
			fmt.Fprintf(os.Stderr, "ERROR: Wrote incomplete message. written=%d, expected=%d\n",
				len(b), written)
			break
		}
		// Notify the receiver
		ch <- nextPktTS
		ctx.sent += 1
		// Update packet fields
		updatePkt(ctx)
		if !morePkts(ctx) {
			break
		}
	}
}

func RecvPkts(wg *sync.WaitGroup, conn *reliable.Conn, ctx *scmpCtx, ch chan time.Time) {
	defer wg.Done()

	b := make(common.RawBytes, mtu)

	start := time.Now()
	nextTimeout := start
	var RTTs []int64 // in nanoseconds
	for {
		nextPktTS, ok := <-ch
		if ok {
			nextTimeout = nextPktTS.Add(*timeout)
			conn.SetReadDeadline(nextTimeout)
		} else if ctx.recv == ctx.sent || nextTimeout.Before(time.Now()) {
			break
		}
		pktLen, err := conn.Read(b)
		if err != nil {
			e, ok := err.(*net.OpError)
			if ok && e.Timeout() {
				continue
			} else {
				fmt.Fprintf(os.Stderr, "ERROR: Unable to read: %v\n", err)
				break
			}
		}
		now := time.Now()
		err = hpkt.ParseScnPkt(ctx.pktR, b[:pktLen])
		if err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: SCION packet parse error: %v\n", err)
			break
		}
		err = validatePkt(ctx)
		if err != nil {
			fmt.Fprintf(os.Stderr, "ERROR: SCMP validation error: %v\n", err)
			break
		}
		ctx.recv += 1
		if *interactive {
			prettyPrint(ctx, pktLen, now)
		} else {
			scmpHdr := ctx.pktR.L4.(*scmp.Hdr)
			rtt := now.UnixNano() - (int64(scmpHdr.Timestamp) * 1000)
			switch info := ctx.infoR.(type) {
			case *scmp.InfoEcho:
				_ = info
				RTTs = append(RTTs, rtt)
			}
		}
	}
	if *interactive {
		fmt.Printf("%d packets transmitted, %d received, %d%% packet loss, time %v\n",
			ctx.sent, ctx.recv, 100-ctx.recv*100/ctx.sent, time.Now().Sub(start))
	} else {
		fpingPrint(ctx, RTTs)
		if *verbose {
			fmt.Printf(" %s timestamp: %d\n", pathStr, time.Now().UnixNano()/1e9)
		} else {
			fmt.Println()
		}
	}
}

func choosePath(interactive bool) *sciond.PathReplyEntry {
	var paths []*sciond.PathReplyEntry
	var pathIndex uint64

	pathMgr := snet.DefNetwork.PathResolver()
	pathSet := pathMgr.Query(local.IA, remote.IA)

	pathSet = policyFilterPaths(pathSet)
	
	if len(pathSet) == 0 {
		return nil
	}

	// check if pathAlgo flag is set
	flagset := make(map[string]bool)
	// record if flags were set or if default value was used
	flag.Visit(func(f *flag.Flag) { flagset[f.Name] = true })

	if flagset["pathAlgo"] {
		aps := pathmgr.AppPathSet{}
		bestPath := pathSelection(pathSet, *pathAlgo)
		aps.Add(bestPath.Entry)
		pathSet = aps
	}

	for _, p := range pathSet {
		paths = append(paths, p.Entry)
	}
	if interactive {
		fmt.Printf("Available paths to %v\n", remote.IA)
		for i := range paths {
			fmt.Printf("[%2d] %s\n", i, paths[i].Path.String())
		}
		reader := bufio.NewReader(os.Stdin)
		for {
			fmt.Printf("Choose path: ")
			pathIndexStr, _ := reader.ReadString('\n')
			var err error
			pathIndex, err = strconv.ParseUint(pathIndexStr[:len(pathIndexStr)-1], 10, 64)
			if err == nil && int(pathIndex) < len(paths) {
				break
			}
			fmt.Fprintf(os.Stderr, "ERROR: Invalid path index, valid indices range: [0, %v]\n", len(paths))
		}
		fmt.Printf("Using path:\n  %s\n", paths[pathIndex].Path.String())
	}
	pathStr = paths[pathIndex].Path.String()
	return paths[pathIndex]
}

func policyFilterPaths(appPaths pathmgr.AppPathSet) pathmgr.AppPathSet {
	policyDenied := strings.Split(*policyDen, ";")
	pp, err := pathmgr.NewPathPredicate(policyDenied[0])
	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to parse path reply: %v\n", err)
	}
	for _, ap := range appPaths {
		if pp.Eval(ap.Entry) {
			delete(appPaths, ap.Key())
		}
	}

	policyRequired := strings.Split(*policyReq, ";")
	pp, err = pathmgr.NewPathPredicate(policyRequired[0])
	for _, ap := range appPaths {
		if !pp.Eval(ap.Entry) {
			delete(appPaths, ap.Key())
		}
	}

	// remove unshortened paths, if policy is set to "short"
	if (len(policyDenied) > 1 && policyDenied[1] == "short") ||
		(len(policyRequired) > 1 && policyRequired[1] == "short") {
		var longPaths []pathmgr.PathKey
		for k, p := range appPaths {
			IAs := make(map[addr.IA]int)
			for _, fwpm := range p.Entry.Path.Interfaces {
				IAs[fwpm.ISD_AS()] += 1
				if IAs[fwpm.ISD_AS()] > 2 {
					longPaths = append(longPaths, k)
				}
			}
		}
		for _, k := range longPaths {
			delete(appPaths, k)
		}
	}
	return appPaths
}

func validate() {
	if local.Host == nil {
		fatal("Invalid local address")
	}
	if remote.Host == nil {
		fatal("Invalid remote address")
	}
	// scmp-tool does not use ports, thus they should not be set
	// Still, the user could set port as 0 ie, ISD-AS,[host]:0 and be valid
	if local.L4Port != 0 {
		fatal("Local port should not be provided")
	}
	if remote.L4Port != 0 {
		fatal("Remote port should not be provided")
	}
}

func setPathAndMtu(pathEntry *sciond.PathReplyEntry) uint16 {
	if pathEntry == nil {
		fatal("No paths available to remote destination")
	}
	remote.Path = spath.New(pathEntry.Path.FwdPath)
	remote.Path.InitOffsets()
	remote.NextHopHost = pathEntry.HostInfo.Host()
	remote.NextHopPort = pathEntry.HostInfo.Port
	return pathEntry.Path.Mtu
}

func setLocalMtu() uint16 {
	// Use local AS MTU when we have no path
	sd := snet.DefNetwork.Sciond()
	c, err := sd.Connect()
	if err != nil {
		fatal("Unable to connect to sciond")
	}
	reply, err := c.ASInfo(addr.IA{})
	if err != nil {
		fatal("Unable to request AS info to sciond")
	}
	// XXX We expect a single entry in the reply
	return reply.Entries[0].Mtu
}

func pathSelection(pathSet pathmgr.AppPathSet, pathAlgo string) *pathmgr.AppPath {
	var selectedPath *pathmgr.AppPath
	var metric float64
	// A path selection algorithm consists of a simple comparision function selecting the best path according
	// to some path property and a metric function normalizing that property to a value in [0,1], where larger is better
	// Available path selection algorithms, the metric returned must be normalized between [0,1]:
	pathAlgos := map[string](func(pathmgr.AppPathSet) (*pathmgr.AppPath, float64)){
		"shortest": selectShortestPath,
		"mtu": selectLargestMTUPath,
	}
	switch pathAlgo {
	case "shortest":
		selectedPath, metric = pathAlgos[pathAlgo](pathSet)
	case "mtu":
		selectedPath, metric = pathAlgos[pathAlgo](pathSet)
	default:
		// Default is to take result with best score
		for _, algo := range pathAlgos {
			cadidatePath, cadidateMetric := algo(pathSet)
			if cadidateMetric > metric {
				selectedPath = cadidatePath
				metric = cadidateMetric
			}
		}
	}
	return selectedPath
}

func selectShortestPath(pathSet pathmgr.AppPathSet) (selectedPath *pathmgr.AppPath, metric float64) {
	// Selects shortest path by number of hops
	for _, appPath := range pathSet {
		if selectedPath == nil || len(appPath.Entry.Path.Interfaces) < len(selectedPath.Entry.Path.Interfaces) {
			selectedPath = appPath
		}
	}
	metric_fn := func(rawMetric []sciond.PathInterface) (result float64) {
		hopCount := float64(len(rawMetric))
		midpoint := 7.0
		result = math.Exp(-(hopCount-midpoint)) / (1 + math.Exp(-(hopCount-midpoint)))
		return result
	}
	return selectedPath, metric_fn(selectedPath.Entry.Path.Interfaces)
}

func selectLargestMTUPath(pathSet pathmgr.AppPathSet) (selectedPath *pathmgr.AppPath, metric float64) {
	// Selects path with largest MTU
	for _, appPath := range pathSet {
		if selectedPath == nil || appPath.Entry.Path.Mtu > selectedPath.Entry.Path.Mtu {
			selectedPath = appPath
		}
	}
	metric_fn := func(rawMetric uint16) (result float64) {
		mtu := float64(rawMetric)
		midpoint := 1500.0
		tilt := 0.004
		result = 1 / (1 + math.Exp(-tilt*(mtu-midpoint)))
		return result
	}
	return selectedPath, metric_fn(selectedPath.Entry.Path.Mtu)
}

func fatal(msg string, a ...interface{}) {
	fmt.Printf("CRIT: "+msg+"\n", a...)
	os.Exit(1)
}
